//
//  NotificationService.swift
//  NotificationServiceDemo
//
//  Created by  Alex on 8/15/19.
//  Copyright © 2019 Alex Frolikoff. All rights reserved.
//

import Foundation

class NotificationService<T> {
	
	func subscribe(completion:@escaping ((T) -> Void )) {
		NotificationCenter.default.addObserver(
			forName: NSNotification.Name(rawValue: String(describing: T.self)),
			object: nil,
			queue: .main) { notification in
				let value = notification.object as! T
				completion(value)
		}
	}
	
	func notify(_ value: T) {
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: String(describing: T.self)), object: value)
	}
}
