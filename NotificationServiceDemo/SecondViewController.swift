//
//  SecondViewController.swift
//  NotificationServiceDemo
//
//  Created by  Alex on 8/15/19.
//  Copyright © 2019 Alex Frolikoff. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

	@IBOutlet weak var `switch`: UISwitch!
	@IBOutlet weak var label: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		NotificationService<Story>().subscribe {[weak self] story in
			self?.label.text = story.bookmarked == true ? "ON" : "OFF"
		}
	}

	@IBAction func switchAction(_ sender: UISwitch) {
		NotificationService<Story>().notify(Story(id: 1, bookmarked: sender.isOn))
	}
}
