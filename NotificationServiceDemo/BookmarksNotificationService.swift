//
//  BookmarksNotificationService.swift
//  NotificationServiceDemo
//
//  Created by  Alex on 8/16/19.
//  Copyright © 2019 Alex Frolikoff. All rights reserved.
//

import Foundation

extension Notification.Name {
	static let bookmarked = Notification.Name("bookmarked")
}

struct Story {
	let id: UInt
	let bookmarked: Bool
}

class BookmarksNotificationService {
	
	func subscribeBookmarking(completion:@escaping ((Bool) -> Void )) {
		NotificationCenter.default.addObserver(forName: .bookmarked, object: nil, queue: .main) { notification in
			let value = notification.object as! Bool
			completion(value)
		}
	}
	
	func notifyBookmarked(_ value: Bool) {
		NotificationCenter.default.post(name: .bookmarked, object: value)
	}
}
