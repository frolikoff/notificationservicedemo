//
//  FirstViewController.swift
//  NotificationServiceDemo
//
//  Created by  Alex on 8/15/19.
//  Copyright © 2019 Alex Frolikoff. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

	@IBOutlet weak var label: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		NotificationService<Story>().subscribe {[weak self] story in
			print(story)
			self?.label.text = story.bookmarked == true ? "ON" : "OFF"
		}
	}
}

